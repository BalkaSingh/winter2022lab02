import java.util.Scanner;
public class PartThree{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		AreaComputations ac = new AreaComputations();
		
		System.out.println("Enter a length for the side of a square (It has to be an integer):");
		int lengthSquare = sc.nextInt();
		System.out.println("Enter a length for the length of a rectangle (It has to be an integer):");
		int lengthRectangle = sc.nextInt();
		System.out.println("Enter a width for the width of a rectangle (It has to be an integer):");
		int widthRectangle = sc.nextInt();
		
		System.out.println("The area of the square : " + AreaComputations.areaSquare(lengthSquare));
		System.out.println("The area of the rectangle : " + ac.areaRectangle(lengthRectangle,widthRectangle));
	}

}